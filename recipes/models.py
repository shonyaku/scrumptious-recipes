from django.db import models


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"'{self.name}' by: {self.author}"


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.abbreviation


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,
        null=True,
    )
    measure = models.ForeignKey(
        "Measure",
        on_delete=models.PROTECT,
        null=True,
    )
    food = models.ForeignKey(
        "FoodItem",
        on_delete=models.PROTECT,
        null=True,
    )

    def __str__(self):
        self.amount = str(self.amount)
        return f"{self.amount} {self.measure} of {self.food} for the recipe {self.recipe}"


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete=models.CASCADE,
        null=True,
    )
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

    def __str__(self):
        print(type(self.recipe))
        return f"{self.recipe} {self.order}: {self.directions}"
